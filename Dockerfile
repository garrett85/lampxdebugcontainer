FROM php:8.2-apache
COPY . /var/www/html
COPY ./httpd-vhost.conf /etc/apache/sites_available/
WORKDIR /var/www/html
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install pdo_mysql \
    && a2enmod rewrite \
    && pecl install xdebug-3.3.1 && docker-php-ext-enable xdebug
    # \
    # && sed -i '/LoadModule rewrite_module/s/^#//g' /usr/local/apache2/conf/httpd.conf \
    # && sed -i 's#AllowOverride [Nn]one#AllowOverride All#' /usr/local/apache2/conf/httpd.conf
